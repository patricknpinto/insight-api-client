$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'insight/api/version'

Gem::Specification.new do |s|
  s.name        = 'insight-api'
  s.version     = Insight::Api::VERSION
  s.authors     = ['Patrick Pinto']
  s.email       = ['hello@patricknpinto.com']
  s.homepage    = 'https://www.gowgates.com.au'
  s.summary     = 'Client for Insight API'
  s.description = 'Client for Insight API'
  s.license     = 'MIT'

  s.files = Dir['{lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'faraday', '< 2'

  s.add_development_dependency 'rubocop', '< 2'
end
