require 'faraday'
require 'insight/api/resources/clients'
require 'insight/api/resources/sales_teams'

module Insight
  module Api
    class Client
      include Insight::Api::Resources::Clients
      include Insight::Api::Resources::SalesTeams

      def initialize
        @connection = Faraday.new(url: Insight::Api.base_url)
      end

      private

      def make_request(path, attributes = {})
        response = @connection.post do |req|
          req.url path
          req.headers['INSIGHT-USERNAME'] = Insight::Api.username
          req.headers['INSIGHT-PASSWORD'] = Insight::Api.password
          req.headers['Content-Type'] = 'application/json'
          req.body = attributes[:body].to_json
        end

        response
      end
    end
  end
end
