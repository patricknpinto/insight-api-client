module Insight
  module Api
    cattr_accessor :host
    cattr_accessor :username
    cattr_accessor :password

    def self.configure
      yield self
    end

    def self.base_url
      "#{host}/Services"
    end
  end
end
