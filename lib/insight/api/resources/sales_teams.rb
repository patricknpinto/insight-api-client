module Insight
  module Api
    module Resources
      module SalesTeams
        def list_sales_teams(params = {})
          make_request('/SalesTeam/List', params: params)
        end
      end
    end
  end
end
