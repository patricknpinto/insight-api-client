module Insight
  module Api
    module Resources
      module Clients
        def list_clients(params = {})
          make_request('Client/List', params: params)
        end

        def create_client(params = {})
          make_request('Client/Create', body: { "Client": params })
        end
      end
    end
  end
end
